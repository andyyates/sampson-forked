#include "Sampler.h"
#include "JackClient.h"
#include "Voice.h"
#include "Voice.h"
#include "Cell.h"
#include "Log.h"

#include <math.h>
#include <stdlib.h>
#include <memory.h>
#include <cstdio>

using std::string;
using std::map;
using std::vector;
using std::endl;

static const size_t _logbufsize = 1024;
static char _logbuf[_logbufsize];
static Log& _log = Log::get_def_inst();

Sampler::Sampler(size_t _num_voices):
num_voices(_num_voices), voices(new Voice[_num_voices])
{
}

Sampler::~Sampler() {
	delete[] voices;
	for (map<string, Sample*>::iterator i = samples.begin(); i != samples.end(); ++i) {
		delete i->second;
	}
	for (vector<Cell*>::iterator i = cells.begin(); i != cells.end(); ++i) {
		delete *i;
	}
}

Sampler& Sampler::addCell(Cell* cell) {
	cells.push_back(cell);
	return *this;
}

void Sampler::processMidiEvent(const jack_midi_event_t& e) {
	switch (e.buffer[0] & 0xF0) {
		case 0x80:
			processNoteOff(e);
			break;
		case 0x90:
			processNoteOn(e);
			break;
	}

	int num_active_voices = 0;
	for (size_t i = 0; i < num_voices; ++i) {
		if (voices[i].isActive())
			++num_active_voices;
	}
	snprintf(_logbuf, _logbufsize, "Active voices: %i", num_active_voices);
}

size_t Sampler::findFreeVoice() const {
	for (size_t i = 0; i < num_voices; ++i) {
		if (!voices[i].isActive()) {
			return i;
		}
	}
	return num_voices;
}

void Sampler::processNoteOn(const jack_midi_event_t& e) {
	for (vector<Cell*>::iterator i = cells.begin(); i != cells.end(); ++i) {
		Cell* cell = *i;
		int channel = e.buffer[0] & 0x0f;
		int key = e.buffer[1];
		int velo = e.buffer[2];
		if (cell->match_note_on(channel, key, velo)) {
			// send keyup to all active voices in this group
			if (cell->get_group()) {
				noteOffByGroup(cell->get_group());
			}
			// find a free voice
			size_t i = findFreeVoice();
			// if no free voice, kill any voice at random (TODO: implement voice priority heuristic)
			if (i >= num_voices) {
				i = rand() % num_voices;
			}
			// start voice
			voices[i].noteOn(channel, key, velo, cell);
		}
	}
}

void Sampler::processNoteOff(const jack_midi_event_t& e) {
	for (size_t i = 0; i < num_voices; ++i) {
		int channel = e.buffer[0] & 0x0f;
		int key = e.buffer[1];
		// int velo = e.buffer[2];
		if (voices[i].isActive() &&
			voices[i].getChannel() == channel &&
			voices[i].getKey() == key) {
			voices[i].noteOff();
		}
	}
}

void Sampler::noteOffByGroup(int group) {
	for (size_t i = 0; i < num_voices; ++i) {
		if (voices[i].isActive() && voices[i].getGroup() == group) {
			voices[i].noteOff(true);
		}
	}
}

void Sampler::process(JackClient& client, jack_nframes_t nframes) {
	float** audio_buffers = (float**)client.get_audio_buffers();
	size_t num_audio_buffers = client.get_num_audio_ports();
	void** midi_buffers = client.get_midi_buffers();
	size_t num_midi_buffers = client.get_num_midi_ports();
	float sample_rate = client.get_sample_rate();
	jack_nframes_t frame_from = 0;
	jack_nframes_t frame_to = 0;
	jack_nframes_t num_midi_events = jack_midi_get_event_count(midi_buffers[0]);
	jack_midi_event_t midi_event;

	// Clear out the buffers, since the voices do additive output.
	for (size_t i = 0; i < num_audio_buffers; ++i) {
		memset(audio_buffers[i], 0, sizeof(float) * nframes);
	}

	// Step through all the MIDI events, processing the buffer in chunks.
	if (num_midi_buffers) {
		for (size_t i = 0; i < num_midi_events; ++i) {
			jack_midi_event_get(&midi_event, midi_buffers[0], i);
			// process audio up to the midi event before processing midi event
			frame_from = frame_to;
			frame_to = midi_event.time;
            processPartially(sample_rate, num_audio_buffers, audio_buffers, frame_from, frame_to - frame_from);
			processMidiEvent(midi_event);
		}
	}
	// Now process the remainder of the audio buffer, after the last MIDI
	// event.
	processPartially(sample_rate, num_audio_buffers, audio_buffers, frame_to, nframes - frame_to);
}

void Sampler::processPartially(jack_nframes_t sample_rate, size_t num_audio_buffers, float** audio_buffers, jack_nframes_t frame0, jack_nframes_t nframes) {
	if (!nframes)
		return;
	for (size_t i = 0; i < num_voices; ++i) {
		voices[i].process(sample_rate, num_audio_buffers, audio_buffers, frame0, nframes);
	}
}

Sample* Sampler::load_sample(const string& filename) {
	if (!samples[filename]) {
		Sample* s = new Sample();
		s->load_sample_file(filename.c_str());
		samples[filename] = s;
	}
	return samples[filename];
}

void Sampler::dump_state(std::ostream& o) const {
	o << "voices: " << num_voices << endl;
	o << samples.size() << " samples:" << endl;
	for (map<string, Sample*>::const_iterator i = samples.begin(); i != samples.end(); ++i) {
		o << i->first << " = " << i->second << endl;
	}

	o << cells.size() << " cells:" << endl;
	for (vector<Cell*>::const_iterator i = cells.begin(); i != cells.end(); ++i) {
		o << "sample: " << (*i)->get_sample() << endl;
		if ((*i)->get_fixed_pitch())
			o << "fixed pitch" << endl;
		else
			o << "ref frequency: " << (*i)->get_ref_freq() << " Hz" << endl;
		o << "key range: " << (*i)->get_key_range().lower << ".." << (*i)->get_key_range().upper << endl;
	}
}
