#include "Envelope.h"
#include "util.h"
#include "Log.h"

EnvelopeParams::EnvelopeParams(float a, float d, float r, float s, float p):
attack_time(a), peak_amp(p), decay_time(d), sustain_amp(s), release_time(r)
{
}

//////////////////

Envelope::Envelope():
params(0), position(0), wallclock_position(0.0f), key_down(false), sustain_reached(false),
release_amp(0.0f), release_time(0.0f), current_amp(0.0f)
{
}

void Envelope::start(const EnvelopeParams* _params) {
	params = _params;
	position = 0;
	wallclock_position = 0.0f;
	key_down = true;
	sustain_reached = false;
	current_amp = 0.0f;
}

void Envelope::release() {
	if (!params->has_release())
		return; // ignore if indefinite release
	key_down = false;
	release_amp = current_amp;
	release_time = params->release_time * release_amp / params->sustain_amp;
	sustain_reached = true;
	position = 0;
	wallclock_position = 0.0f;
}

float Envelope::get_amp() const {
	return current_amp;
}

void Envelope::next_sample(jack_nframes_t sample_rate) {
	if (!is_active()) {
		current_amp = 0.0f;
		return;
	}

	++position;
	wallclock_position = (float)position / (float)sample_rate;

	if (!key_down) {
		// Release phase
		current_amp = clamp_to_range(release_amp * (1.0f - wallclock_position / release_time), 0.0f, release_amp);
	}
	else if (sustain_reached) {
		// Sustain phase
		current_amp = params->sustain_amp;
	}
	else if (wallclock_position <= params->attack_time) {
		// Attack phase
		current_amp = clamp_to_range(
			params->peak_amp * wallclock_position / params->attack_time,
			0.0f, params->peak_amp);
	}
	else if (wallclock_position >= params->get_sustain_time()) {
		// Enter sustain phase
		sustain_reached = true;
		current_amp = params->sustain_amp;
	}
	else {
		// Decay phase
		current_amp = clamp_to_range(
			params->peak_amp + 
			(params->sustain_amp - params->peak_amp) *
			((wallclock_position - params->attack_time) /
			 params->decay_time),
			params->sustain_amp,
			params->peak_amp);
	}
}

bool Envelope::is_active() const {
	if (!params)
		return false;
	if (!key_down) {
		return wallclock_position <= release_time;
	}
	return true;
}

