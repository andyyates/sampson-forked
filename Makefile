EXECUTABLE=sampson
CFLAGS+=-W -Wall -std=c++11

ifdef DEBUG
CFLAGS+=-g -ggdb
else
CFLAGS+=-Ofast
CFLAGS+=-ffast-math -fexpensive-optimizations -ffloat-store
CFLAGS+=-fearly-inlining -finline-functions-called-once
CFLAGS+=-fgcse -fgcse-after-reload
CFLAGS+=-funroll-loops -funswitch-loops
CFLAGS+=-funsafe-loop-optimizations
CFLAGS+=-funsafe-math-optimizations
CFLAGS+=-march=x86-64
endif

ifdef PROFILE
CFLAGS+=-pg
else
CFLAGS+=-fomit-frame-pointer
endif

SOURCES=$(wildcard *.cpp)
HEADERS=$(wildcard *.h)
LOADLIBES=jack sndfile
OBJECTS=$(patsubst %.cpp,%.o,$(SOURCES))
LDFLAGS+=$(patsubst %,-l%,$(LOADLIBES))
INSTALL_PREFIX=/usr/local

all: depend $(EXECUTABLE)

install: all
	install $(EXECUTABLE) $(INSTALL_PREFIX)/bin/

%.o: %.cpp
	$(CXX) $(CFLAGS) $(CXXFLAGS) -c $< -o $@

$(EXECUTABLE): $(OBJECTS)
	$(CXX) $(CFLAGS) $(CXXFLAGS)  $(OBJECTS) -o $(EXECUTABLE) $(LDFLAGS)

depend: Makefile $(SOURCES) $(HEADERS)
	$(CXX) $(SOURCES) $(HEADERS) -MM > $@

clean:
	$(RM) $(OBJECTS)

veryclean: clean
	$(RM) depend $(EXECUTABLE)

.PHONY: clean veryclean

include depend
