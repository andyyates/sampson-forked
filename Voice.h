#pragma once

#include <jack/jack.h>

#include "Envelope.h"

class Cell;
class JackClient;

class Voice {
	private:
		const Cell* cell;
		int channel;
		int key;
		int velocity;
		bool key_down;
		jack_nframes_t sample_pos;
		float sample_pos_frac;
		float delta_phi;
		jack_nframes_t buffered_sample_rate;
		Envelope envelope;
	public:
		Voice();
		int getChannel() const { return channel; }
		int getKey() const { return key; }
		int getGroup() const;
		float getFrequency() const;
		float getDeltaPhi(jack_nframes_t sample_rate) const;

		void noteOn(int _channel, int _key, int _velocity, const Cell* _cell);
		void noteOff(bool force = false);
		bool isActive() const;

		void process(jack_nframes_t sample_rate, size_t num_audio_buffers, float** audio_buffers, jack_nframes_t frame0, jack_nframes_t nframes);
};
