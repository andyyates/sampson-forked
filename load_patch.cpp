#include "load_patch.h"
#include "util.h"
#include "Cell.h"
#include "Sampler.h"
#include <fstream>
#include <string>
#include <iostream>
#include <algorithm>
#include <libgen.h>
#include <unistd.h>
#include <string.h>

using namespace std;

// trim from start
static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

static int parseKey(string s) {
	if (is_numeric(s[0])) {
		// clog << "Parse as numeric key specifier: " << s << endl;
		return atol(s.c_str());
	}
	if (is_note_basename(s[0])) {
		// clog << "Parse as symbolic key specifier: " << s << endl;
		int base = s.at(0);
		int acc;
		string octstr;
		int oct;

		switch (s.at(1)) {
			case 'b':
				acc = -1;
				octstr = s.substr(2);
				break;
			case '#':
				acc = 1;
				octstr = s.substr(2);
				break;
			default:
				acc = 0;
				octstr = s.substr(1);
				break;
		}
		oct = atol(octstr.c_str());
		// clog << base << ":" << acc << "+" << oct << endl;
		return note_to_midikey(base, acc, oct);
	}
	return 0;
}

static float parseFreq(string s) {
	if (s.length() > 2 && s.substr(s.length()-2) == "Hz") {
		// clog << "Parse as frequency: " << s << endl;
		return atof(s.substr(0, s.length()-2).c_str());
	}
	else {
		return midikey_to_freq(parseKey(s));
	}
}

static float parseAmp(string s) {
	if (s.length() > 2 && s.substr(s.length()-2) == "dB") {
		// parse as decibels
		float db = atof(s.substr(0, s.length()-2).c_str());
		return db_to_amp(db);
	}
	else {
		return atof(s.c_str());
	}
}

static float parsePan(string s) {
	if (s == "left" || s == "l" || s == "L")
		return -1.0;
	if (s == "right" || s == "r" || s == "R")
		return 1.0;
	if (s == "center" || s == "c" || s == "C")
		return 0.0;
	return atof(s.c_str());
}

static void loadCell(Sampler& s, istream& fin) {
	Cell* cell = new Cell();
	string token;
	while (fin.good()) {
		fin >> token;
		if (token == "--") {
			if (cell->get_sample()) {
				s.addCell(cell);
			}
			return;
		}
		else if (token == "file") {
			string filename;
			Sample* sample;

			getline(fin, filename);
			ltrim(filename);
			clog << "'" << filename << "'" << endl;
			sample = s.load_sample(filename);
			cell->set_sample(sample);
		}
		else if (token == "refkey") {
			string k;
			fin >> k;
			cell->set_ref_freq(parseFreq(k));
		}
		else if (token == "keyrange") {
			string low, high;
			fin >> low;
			fin >> high;
			// +1 to compensate for inclusive / exclusive ranges.
			cell->set_key_range(parseKey(low), parseKey(high) + 1);
		}
		else if (token == "velorange") {
			int low, high;
			fin >> low;
			fin >> high;
			cell->set_velo_range(low, high);
		}
		else if (token == "velorangex") {
			int low, high, xfade_low, xfade_high;
			fin >> xfade_low;
			fin >> low;
			fin >> high;
			fin >> xfade_high;
			cell->set_velo_range(low, high, xfade_low, xfade_high);
		}
		else if (token == "noteoff") {
			cell->set_accept_note_off(true);
		}
		else if (token == "nonoteoff") {
			cell->set_accept_note_off(false);
		}
		else if (token == "fixedpitch") {
			cell->set_fixed_pitch(true);
		}
		else if (token == "group") {
			int group;
			fin >> group;
			cell->set_group(group);
		}
		else if (token == "pan") {
			string pan;
			fin >> pan;
			cell->set_pan(parsePan(pan));
		}
		else if (token == "amp") {
			string amp;
			fin >> amp;
			cell->set_amp(parseAmp(amp));
		}
		else if (token == "att") {
			fin >> cell->get_envelope_params().attack_time;
		}
		else if (token == "dec") {
			fin >> cell->get_envelope_params().decay_time;
		}
		else if (token == "sus") {
			fin >> cell->get_envelope_params().sustain_amp;
		}
		else if (token == "rel") {
			fin >> cell->get_envelope_params().release_time;
		}
		else if (token.length() > 0 && token.at(0) == ';') {
			string dummy;
			getline(fin, dummy);
		}
	}
	if (cell->get_sample()) {
		s.addCell(cell);
	}
}

Sampler& loadPatch(Sampler& s, const char* fn) {
	// we should cd into the kit's basedir to make sure we can find all those
	// files	
	char dirbuf[1024];
	char* fndup = strdup(fn);
	char* basedir = NULL;
	ifstream fin(fn);
	basedir = dirname(fndup);
	getcwd(dirbuf, 1024);
	if (basedir && *basedir)
		chdir(basedir);
	free(fndup);
	loadPatch(s, fin);
	chdir(dirbuf);
	return s;
}

Sampler& loadPatch(Sampler& s, istream& fin) {
	while (fin.good()) {
		loadCell(s, fin);
	}
	return s;
}
