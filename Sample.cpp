#include "Sample.h"
#include "Log.h"

#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <sndfile.h>
#include <iostream>
#include <fstream>

Sample::Sample():
sample_rate(1), nframes(0), sample_data(0), nchannels(0)
{
}

Sample::~Sample() {
	free(sample_data);
}

float Sample::get_sample_value(jack_nframes_t sample_pos, float sample_pos_frac, size_t channel) const {
	static float buffer[2];
	get_sample_values_raw(buffer, sample_pos, channel);
	return buffer[0] + (buffer[1] - buffer[0]) * sample_pos_frac;
}

void Sample::get_sample_values_raw(float* buffer, jack_nframes_t sample_pos, size_t channel) const {
	static jack_nframes_t sample_index;
	sample_index = sample_pos * nchannels + channel;

	if (nchannels && sample_data) {
		channel %= nchannels;
		if (sample_pos < nframes) {
			buffer[0] = sample_data[sample_index];
			if (sample_pos < nframes - 1) {
				buffer[1] = sample_data[sample_index + nchannels];
			}
			else {
				buffer[1] = 0.0f;
			}
		}
		else {
			buffer[0] = 0.0f;
			buffer[1] = 0.0f;
		}
	}
	else {
		buffer[0] = 0.0f;
		buffer[1] = 0.0f;
	}
}

void Sample::load_sample(jack_nframes_t _nframes, float* _data, jack_nframes_t _sample_rate, size_t _nchannels) {
	free(sample_data);
	if (_data && _nframes && _nchannels) {
		size_t nbytes = sizeof(float) * _nframes * _nchannels;
		sample_data = (float*)malloc(nbytes);
		memcpy((void*)sample_data, (void*)_data, nbytes);
	}
	else {
		sample_data = 0;
	}
	nframes = _nframes;
	nchannels = _nchannels;
	sample_rate = _sample_rate;
}

void Sample::load_sample_file(const char* filename) {
	Log& log = Log::get_def_inst();
	SF_INFO sfinfo;
	SNDFILE* sndfile;
	memset(&sfinfo, 0, sizeof(SF_INFO));
	sndfile = sf_open(filename, SFM_READ, &sfinfo);
	if (sndfile == NULL) {
		log.putstr("File not found or not a sound file: ");
		log.putstr(filename);
		log.putln("");
	}
	free(sample_data);
	size_t nbytes = sfinfo.frames * (size_t)sfinfo.channels * sizeof(float);
	sample_data = (float*)malloc(nbytes);
	nframes = sfinfo.frames;
	nchannels = (size_t)sfinfo.channels;
	sample_rate = (jack_nframes_t)sfinfo.samplerate;
	sf_read_float(sndfile, sample_data, nframes * nchannels);
	sf_close(sndfile);
	std::clog << "Loaded sample " << filename << " (" << nframes << " frames, " << nchannels << " channels)" << std::endl;
}
